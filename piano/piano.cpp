#include "stdafx.h"
#include <glut.h>
#include "3ds.h"
#include "Texture.h"
#include <time.h>
#include "fmod.h"                              //// 音频库的头文件
#pragma comment(lib, "fmodvc.lib")	          // 把音频库加入到链接器中
#define N 1000
FSOUND_STREAM *mp3back1;
FSOUND_STREAM *mp3back2;
FSOUND_STREAM *mp3back3;
FSOUND_STREAM *mp3back4;
FSOUND_STREAM *mp3back5;
FSOUND_STREAM *mp3back6;
FSOUND_STREAM *mp3back7;
FSOUND_STREAM *mp3backc1;
FSOUND_STREAM *mp3backc2;
FSOUND_STREAM *mp3backc3;
FSOUND_STREAM *mp3backc4;
FSOUND_STREAM *mp3backc5;
FSOUND_STREAM *mp3backc6;
FSOUND_STREAM *mp3backc7;

void music(int mu);
int mu = 0;
float musictime1 = 0;
float musictime2 = 0;

void drawball();
int ba = 0;

void reshape(int w,int h);
void init();
void display();
void mytime(int value);

void pianokey1(int n,float m);// n代表第几个。m代表琴键宽
void pianokey2(int n,float m);// n代表第几个。m代表琴键宽
void pianokey3(int n,float m);// n代表第几个。m代表琴键宽

void pianobalckkey(int n,float m);// n代表第几个。m代表琴键宽

void suiji();
void stopmusic();
void drawdesk();
void Keyboard(unsigned char key, int x, int y);

void play(int p);//匹配按键
void resultfont(int a);

//字体
void selectFont(int size, int charset, const char* face); //选择字体
void drawCNString(const char* str); //生成中文字体函数
void drawdeak();
int inner=1,outer=8;   //torus's inner & outer radius

int ww=0,hh=0;

float colorflag=1;
float x11=0,x12=0,x13=0,x14=0,x15=0,x16=0,x17=0,x21=0,x22=0,
	x23=0,x24=0,x25=0,x26=0,x27=0,x31=0,x32=0,x33=0,x34=0,x35=0,x36=0,
	x37=0;//琴键我们来旋转吧
float s=1;
float eyex=0,eyey=6,eyez=18;  //eye point initial position
float atx=0,aty=4,atz=0;  //at point initial position
double angle = 0;
double anglez =0;
double anglestep = 10*3.14/180;		//角度和弧度的换算
float walkstep = 0.1*s;


int flag = 0;		//启动画面判断

float x = 3, y = 5;//矩形位置
int k = 0;			//数据变量
int keyflag = 0;//控制变量进入模式   =0是练习模式  =1是弹奏小星星
float line[N][2];	//小气泡坐标
float colorball[N][3];//小气泡颜色随机

int star[14] = {1,1,5,5,6,6,5,4,4,3,3,2,2,1};//小星星乐谱
int playerkey[14];//存放按键
int keyp = 0;//当前已按键数
int mathflag = 0;		//没玩：0  输：1  赢2
int ballflag =0;
float threat =0;

GLuint textureid[4]; // 定义纹理变量



int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
 
	char *argv[] = {"hello ", " "};
	int argc = 2; // must/should match the number of strings in argv


	glutInit(&argc, argv);    //初始化GLUT库；

	

		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB|GLUT_DEPTH);   

	glutInitWindowSize(600, 600);
	glutInitWindowPosition(1024 / 2 - 250, 768 / 2 - 250);
	glutCreateWindow("玩转钢琴");  //创建窗口，标题为“…”；
	 glutReshapeFunc(reshape);
	init();
	glutDisplayFunc(display);  //用于绘制当前窗口；
	glutKeyboardFunc(Keyboard);  //注册键盘响应回调函数
	glutTimerFunc(1000,mytime,10);
	glutMainLoop();   //表示开始运行程序，用于程序的结尾；

	return 0;
}

  void reshape(int w,int h)
{   
	


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (flag==0)
		gluOrtho2D(-10,10,-10*h/w,10*h/w);  //设置裁剪窗口大小
	else
	 gluPerspective(90,w/h,5,110);

	glViewport(0,0,w,h);
	glMatrixMode(GL_MODELVIEW);
	ww=w;
	hh=h;   
	
}
void init()
{
	glClearColor(1,1,1,1);

	srand((unsigned)time(NULL));

	
	BuildTexture("wallpaper.jpg", textureid[0]);
	glLineWidth(2.0);


	if (FSOUND_Init(44100, 32, 0))			// 把声音初始化为44khz
	{
		mp3back1 = FSOUND_Stream_OpenFile("1.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3back2 = FSOUND_Stream_OpenFile("2.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3back3 = FSOUND_Stream_OpenFile("3.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3back4 = FSOUND_Stream_OpenFile("4.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3back5 = FSOUND_Stream_OpenFile("5.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3back6 = FSOUND_Stream_OpenFile("6.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3back7 = FSOUND_Stream_OpenFile("7.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3backc1 = FSOUND_Stream_OpenFile("c1.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3backc2 = FSOUND_Stream_OpenFile("c2.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3backc3 = FSOUND_Stream_OpenFile("c3.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3backc4 = FSOUND_Stream_OpenFile("c4.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3backc5 = FSOUND_Stream_OpenFile("c5.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3backc6 = FSOUND_Stream_OpenFile("c6.mp3", FSOUND_LOOP_NORMAL, 0);
		mp3backc7 = FSOUND_Stream_OpenFile("c7.mp3", FSOUND_LOOP_NORMAL, 0);
	}

	

  }

void mytime(int value)
{
	threat+=0.5;
	for (int i = 0; i < k; i++) {
		if ((rand() % 2 + 1) % 2 == 0) {
			line[i][0] += (rand() % 2 + 1) % 2;
			line[i][1] += (rand() % 1 + 1) % 2;
		}
		else {
			line[i][0] -= (rand() % 2 + 1) % 2;
			line[i][1] += (rand() % 1 + 1) % 2;
		}
	}
	musictime1 += 0.1;
	if (musictime1 != musictime2) {
		stopmusic();
	}


	glutPostRedisplay();

	glutTimerFunc(1000,mytime,10);
}

void display()
 { 

	   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity(); 
													

		if(flag==0){//启动画面
			


			glColor3f(44/255.0,44/255.0,90/255.0);		//背景
			glRectf(10,10,-10,-10);

			selectFont(25, GB2312_CHARSET, "楷体_GB2312"); //设置字体楷体48号字
			glColor3f(1,1,1);
			glRasterPos2f(-2,-2); 
			drawCNString("玩趣钢琴");  
			glRasterPos2f(-2.1,-3.5);  
			drawCNString("Enter进入"); 
			glRasterPos2f(-4.1, -5);  
			drawCNString("1601050043 刘俊颖");  
			glRasterPos2f(-4.1, -6.5);  
			drawCNString("1601050085 张颖姗");  
			glRasterPos2f(-8.5, -8);
			drawCNString("按J进入演奏《小星星》模式，按F进入练习模式");
			glRasterPos2f(-8.5, -9.5);
			drawCNString("按数字'1-7'或字母'z,x,c,v,b,n,m'可弹奏钢琴");

			for(int i = 0 ;i<14;i++){
				glColor3f(1,1,1);		//琴键
				glRectf(-10+(i*1.5)+0.08,0,-8.5+(i*1.5),10);
			}

			for(int i = 0 ;i<21;i=i+7){
				
				glColor3f(0,0,0);		//琴键
				glRectf(-9.25+(i*1.5)+0.08,3,-7.75+(i*1.5),10);

				glColor3f(0,0,0);		//琴键
				glRectf(-9.25+((i+1)*1.5)+0.08,3,-7.75+((i+1)*1.5),10);
	
				glColor3f(0,0,0);		//琴键
				glRectf(-9.25+((i+3)*1.5)+0.08,3,-7.75+((i+3)*1.5),10);


				glColor3f(0,0,0);		//琴键
				glRectf(-9.25+((i+4)*1.5)+0.08,3,-7.75+((i+4)*1.5),10);

				glColor3f(0,0,0);		//琴键
				glRectf(-9.25+((i+5)*1.5)+0.08,3,-7.75+((i+5)*1.5),10);

				
			}

			glutSwapBuffers();  //双缓冲下的刷新方法

		}



		else{		/*开始游戏*/
			
		   glMatrixMode(GL_PROJECTION);
	       glLoadIdentity();
		   glEnable(GL_DEPTH_TEST);
		   gluPerspective(90,ww/hh,5,110);
		   glMatrixMode(GL_MODELVIEW);
	       glLoadIdentity();

			 gluLookAt(eyex,eyey,eyez,atx,aty,atz,0,1,0); //视点和目标点不能重合。

			 if (mathflag == 1) {
				 selectFont(24, GB2312_CHARSET, "楷体_GB2312"); //设置字体楷体48号字
				 glColor3f(0, 0, 1);
				 glRasterPos3f(8, 8, 2);  //在世界坐标250,250处定位首字位置
				 drawCNString("你输了！");  //写字“Hello，大家好”
				 keyflag = 0;
			 }
			 else if (mathflag == 2) {
				 selectFont(24, GB2312_CHARSET, "楷体_GB2312"); //设置字体楷体48号字
				 glColor3f(0, 0, 1);
				 glRasterPos3f(8, 8, 2);  //在世界坐标250,250处定位首字位置
				 drawCNString("你赢了！");  //写字“Hello，大家好”
				 keyflag = 0;
			 }
			 

		glPushMatrix();				//地板
				glColor3f(228/255.0,195/255.0,128/255.0);
				glTranslated(0,-4.9,0);
				glScalef(40,0.1,30);
				glutSolidCube(1);
		glPopMatrix();

		glPushMatrix();	
		glColor3f(225/255.0,205/255.0,156/255.0);//灯
				glTranslated(0,18,0);
				glRotatef(90,1,0,0);
				glTranslated(0,-18,0);
		glTranslated(0,18,0);
		glutSolidTorus(0.5,3.5,30,50);
		glPopMatrix();

		glPushMatrix();	
		glColor3f(125/255.0,153/255.0,215/255.0);//灯(绳)
		glTranslated(0,16,0);
		glScalef(0.08,8,0.08);
		glutSolidCube(1);
		glPopMatrix();


		glPushMatrix();	
		glColor3f(205/255.0,203/255.0,245/255.0);//灯(球)
		glTranslated(0,11,0);
		glutSolidSphere(1.5,20,20);
		glPopMatrix();

		glPushMatrix();	
		glColor3f(125/255.0,153/255.0,215/255.0);//灯(球)
				glTranslated(0,11,0);
				glRotatef(threat,1,1,0);
				glTranslated(0,-11,0);
		glTranslated(0,11,0);
		glutWireSphere(2,15,15);
		glPopMatrix();

		for(int i=1;i<=4;i++){
		glPushMatrix();	
		glColor3f(220/255.0,116/255.0,115/255.0);//灯(横绳)
				glTranslated(0,18,0);
				glRotatef(i*45,0,1,0);
				glTranslated(0,-18,0);
		glTranslated(0,18,0);
		glScalef(6.8,0.08,0.08);
		glutSolidCube(1);
		glPopMatrix();
		}

		glPushMatrix();				//天花板
				glColor3f(228/255.0,195/255.0,128/255.0);
				
				glTranslated(0,20,0);
				glScalef(40,0.1,30);
				glutSolidCube(1);
		glPopMatrix();


		

		glPushMatrix();			//后墙228, 195, 128
			glPushMatrix();		
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, textureid[0]);  //绑定指定纹理
				glBegin(GL_QUADS);
				glTexCoord2f(0.0f, 0.0f);
				glVertex3d(-20,-4.9,-15); 
				glTexCoord2f(1.0f, 0.0f);
				glVertex3d(20,-4.9,-15); 
				glTexCoord2f(1.0f, 1.0f);
				glVertex3d(20,20,-15); 
				glTexCoord2f(0.0f, 1.0f);
				glVertex3d(-20,20,-15); 
				glEnd();
			glPopMatrix();


				glDisable(GL_TEXTURE_2D);
		glPopMatrix();

		glPushMatrix();			//左墙
			glPushMatrix();		
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, textureid[0]);  //绑定指定纹理
				glBegin(GL_QUADS);
				glTexCoord2f(0.0f, 0.0f);
				glVertex3d(-20,-4.9,15); 
				glTexCoord2f(1.0f, 0.0f);
				glVertex3d(-20,-4.9,-15); 
				glTexCoord2f(1.0f, 1.0f);
				glVertex3d(-20,20,-15); 
				glTexCoord2f(0.0f, 1.0f);
				glVertex3d(-20,20,15); 
				glEnd();
			glPopMatrix();


				glDisable(GL_TEXTURE_2D);

				/*glScalef(0.1,250,150);
				glTranslated(-180,0,0);
				glutSolidCube(1);*/
		glPopMatrix();

		glPushMatrix();			//右墙
			glPushMatrix();		
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, textureid[0]);  //绑定指定纹理
				glBegin(GL_QUADS);
				glTexCoord2f(0.0f, 0.0f);
				glVertex3d(20,-4.9,15); 
				glTexCoord2f(1.0f, 0.0f);
				glVertex3d(20,-4.9,-15); 
				glTexCoord2f(1.0f, 1.0f);
				glVertex3d(20,20,-15); 
				glTexCoord2f(0.0f, 1.0f);
				glVertex3d(20,20,15); 
				glEnd();
			glPopMatrix();

			glDisable(GL_TEXTURE_2D);
			
		glPopMatrix();



//hello!I'm piano .Here I will scale
glPushMatrix();
		
	   glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glScalef(9,10,2);
	     glutSolidCube(1);  //绘制钢琴（背）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	    glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(0,-0.1,3);
		 glScalef(9,0.0001,4);
	     glutSolidCube(1);  //绘制钢琴（桌右）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();
	   
	  glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(0,0,5.1);
		 glScalef(9,0.5,0.1);
	     glutSolidCube(1);  //绘制钢琴（桌前）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(-4.5000001,0,3);
		 glScalef(0.0000001,0.5,4);
	     glutSolidCube(1);  //绘制钢琴（桌左）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(4.5000001,0,3);
		 glScalef(0.0000001,0.5,4);
	     glutSolidCube(1);  //绘制钢琴（桌右）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();



	    glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(4.25,-2.625,5);
		 glScalef(0.5,4.75,0.5);//5-2.375
	     glutSolidCube(1);  //绘制钢琴（右脚）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	    glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(-4.25,-2.625,5);
		 glScalef(0.5,4.75,0.5);//5-2.375
	     glutSolidCube(1);  //绘制钢琴（左脚）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	    glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(-4.25,-4.75,2);
		 glScalef(0.5,0.5,6);
	     glutSolidCube(1);  //绘制钢琴（左腿）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	    glPushMatrix();
	     glColor3f(0.2,0.2,0.2); 

         //your code here;
		 glTranslated(4.25,-4.75,2);
		 glScalef(0.5,0.5,6);
	     glutSolidCube(1);  //绘制钢琴（右腿）

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   pianokey1(0,-4.275);
	   pianokey2(7,-4.275);
	   pianokey3(14,-4.275);
	   
	   pianobalckkey(0,-4.07);
	   pianobalckkey(7,-4.07);
	   pianobalckkey(14,-4.07);



	    drawdesk();

		drawball();//确定新写的drawball里有个茶壶位于椅子的右前腿出现是没有问题的

glPopMatrix();


	 glutSwapBuffers();  //双缓冲下的刷新方法
}
	  


	  

	   
	   


     
} 

void pianokey1(int n,float m){//-4.275
	glPushMatrix();
	     glColor3f(1,1,1); 

//your code here;

		//旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x11,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
		 glTranslated(m+n*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.1

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x12,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+1)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.2

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x13,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+2)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.3

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x14,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+3)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.4

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x15,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+4)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.5

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x16,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+5)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.6

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x17,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+6)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.7

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();
}

void pianokey2(int n,float m){
	glPushMatrix();
	     glColor3f(1,1,1); 

//your code here;

		//旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x21,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
		 glTranslated(m+n*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.1

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x22,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+1)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.2

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x23,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+2)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.3

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x24,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+3)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.4

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x25,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+4)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.5

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x26,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+5)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.6

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x27,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+6)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.7

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();
}

void pianokey3(int n,float m){
	glPushMatrix();
	     glColor3f(1,1,1); 

//your code here;

		//旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x31,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
		 glTranslated(m+n*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.1

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x32,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+1)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.2

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x33,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+2)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.3

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();


	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x34,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+3)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.4

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x35,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+4)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.5

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x36,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+5)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.6

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(1,1,1); 
		 //旋转三部曲
			glTranslated(m+n*0.43,0.3,1.1);
			glRotatef(x37,1,0,0);
			glTranslated(-m-n*0.43,-0.3,-1.1);
		//三部曲结束
         //your code here;
		 glTranslated(m+(n+6)*0.43,0.3,3.1);
		 glScalef(0.43,0.1,4);
	     glutSolidCube(1);  //琴键1.7

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();
}

void pianobalckkey(int n,float m){//-4.07+0.44
	glPushMatrix();
	     glColor3f(0,0,0); 
//your code here;
		 glTranslated(m+n*0.43,0.39,2.4);
		 glScalef(0.22,0.08,2.6);
	     glutSolidCube(1);  //琴键1.1黑

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	glPushMatrix();
	     glColor3f(0,0,0); 
//your code here;
		 glTranslated(m+(n+1)*0.43,0.39,2.4);
		 glScalef(0.22,0.08,2.6);
	     glutSolidCube(1);  //琴键1.2黑

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();



	   glPushMatrix();
	     glColor3f(0,0,0); 
//your code here;
		 glTranslated(m+(n+3)*0.43,0.39,2.4);
		 glScalef(0.22,0.08,2.6);
	     glutSolidCube(1);  //琴键1.4黑

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(0,0,0); 
//your code here;
		 glTranslated(m+(n+4)*0.43,0.39,2.4);
		 glScalef(0.22,0.08,2.6);
	     glutSolidCube(1);  //琴键1.5黑

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(0,0,0); 
//your code here;
		 glTranslated(m+(n+5)*0.43,0.39,2.4);
		 glScalef(0.22,0.08,2.6);
	     glutSolidCube(1);  //琴键1.6黑

		 glColor3f(0.3,0.3,0.3);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	  
}

void Keyboard(unsigned char key, int x, int y)
{
  
	switch(key){
		case 13:
			flag=1;
			break;

		case 'r':
			flag=1;
			break;
			case 'W':
         case 'w':// 向前直走
			//your code Here
			 eyex = eyex - walkstep*sin(angle);
			 eyez = eyez - walkstep*cos(angle);
			 atx = atx - walkstep*sin(angle);
			 atz = atz - walkstep*cos(angle);
	        break;
case 'S':
         case 's'://向后退
			//your code Here
			 eyex = eyex + walkstep*sin(angle);
			 eyez = eyez + walkstep*cos(angle);
			 atx = atx + walkstep*sin(angle);
			 atz = atz + walkstep*cos(angle);
		           break;
case 'A':
         case 'a'://左看
			 //your code Here
			 angle = angle + anglestep;
			 atx = eyex - s*sin(angle);
			 atz = eyez - s*cos(angle);
		           break;
case 'D':
         case 'd'://右看
			 //your code Here
			 angle = angle - anglestep;
			 atx = eyex - s*sin(angle);
			 atz = eyez - s*cos(angle);
                  break;   
      

case 'Q':
         case 'q'://上看
			 //your code Here
			 anglez = anglez + anglestep;
			 aty = eyey + s*sin(anglez);
			 atz = eyez - s*cos(anglez);
                     break;   

case 'E':
         case 'e'://下看
			 //your code Here
			 anglez = anglez - anglestep;
			 aty = eyey + s*sin(anglez);
			 atz = eyez - s*cos(anglez);
                  break; 
	//确定模式
		 case'F':
		 case'f':
			 keyflag = 0;
			 mathflag = 0;
			 break;
		 case'J':
		 case'j':
			 keyflag = 1;
			 mathflag = 0;
			 keyp = 0;
			 break;

      //1234567
		 case'1':
			 stopmusic();
			 x21 = 4;
			 music(1);
			 ballflag = 1;
			 k++; 
			 suiji();
			 if (keyflag == 1) {
				 playerkey[keyp]=1;
				 keyp++;
				 
			 }
			 break;
		 case'2':
			 stopmusic();
			 x22 = 4;
			 music(2);
			 ballflag = 1;
			 k++; 
			 suiji();
			 if (keyflag == 1) {
				 playerkey[keyp] = 2;
				 keyp++;
			 }
			 break;
		 case'3':
			 stopmusic();
			 x23 = 4;
			 music(3);
			 ballflag = 1;
			 k++; 
			 suiji();
			 if (keyflag == 1) {
				 playerkey[keyp] = 3;
				 keyp++;
			 }
			 break;
		 case'4':
			 stopmusic();
			 x24 = 4;
			 music(4);
			 ballflag = 1;
			 k++; 
			 suiji();
			 if (keyflag == 1) {
				 playerkey[keyp] = 4;
				 keyp++;
			 }
			 break;
		 case'5':
			 stopmusic();
			 x25 = 4;
			 music(5);
			 ballflag = 1;
			 k++;
			 suiji();
			 if (keyflag == 1) {
				 playerkey[keyp] = 5;
				 keyp++;
			 }
			 break;
		 case'6':
			 stopmusic();
			 x26 = 4;
			 music(6);
			 ballflag = 1;
			 k++;
			 suiji();
			 if (keyflag == 1) {
				 playerkey[keyp] = 6;
				 keyp++;
			 }
			 break;
		 case'7':
			 stopmusic();
			 x27 = 4;
			 music(7);
			 k++;
			 ballflag = 1;
			 if (keyflag == 1) {
				 playerkey[keyp] = 7;
				 keyp++;
			 }
			 break;
		 case'Z':
		 case'z':
			 stopmusic();
			 x11 = 4;
			 music(8);
			 ballflag = 1;
			 k++;
			 suiji();
			 break;
		 case'X':
		 case'x':
			 stopmusic();
			 x12 = 4;
			 music(9);
			 ballflag = 1;
			 k++;
			 suiji();
			 break;
		 case'C':
		 case'c':
			 stopmusic();
			 x13 = 4;
			 music(10);
			 ballflag = 1;
			 k++;
			 suiji();
			 break;
		 case'V':
		 case'v':
			 stopmusic();
			 x14 = 4;
			 music(11);
			 ballflag = 1;
			 k++;
			 suiji();
			 break;
		 case'B':
		 case'b':
			 stopmusic();
			 x15 = 4;
			 music(12);
			 ballflag = 1;
			 k++;
			 suiji();
			 break;
		 case'N':
		 case'n':
			 stopmusic();
			 x16 = 4;
			 music(13);
			 ballflag = 1;
			 k++;
			 suiji();
			 break;
		 case'M':
		 case'm':
			 stopmusic();
			 x17 = 4;
			 music(14);
			 ballflag = 1;
			 k++;
			 suiji();
			 break;
	
	
	}
	if (keyp == 14) {
		play(0);
	}
    glutPostRedisplay();

}


/************************************************************************/
/* 选择字体函数                                                                     */
/************************************************************************/
void selectFont(int size, int charset, const char* face) 
{
	HFONT hFont = CreateFontA(size, 0, 0, 0, FW_MEDIUM, 0, 0, 0,
	charset, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
	DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, face);
	HFONT hOldFont = (HFONT)SelectObject(wglGetCurrentDC(), hFont);
	DeleteObject(hOldFont);
}

/************************************************************************/
/* 生成中文字体函数                                                                     */
/************************************************************************/
void drawCNString(const char* str)
{
	int len, i;
	wchar_t* wstring;
	HDC hDC = wglGetCurrentDC();
	GLuint list = glGenLists(1);

	// 计算字符的个数
	// 如果是双字节字符的（比如中文字符），两个字节才算一个字符
	// 否则一个字节算一个字符
	len = 0;
	for(i=0; str[i]!='\0'; ++i)
	{
		if( IsDBCSLeadByte(str[i]) )
			++i;
		++len;
	}

	// 将混合字符转化为宽字符
	wstring = (wchar_t*)malloc((len+1) * sizeof(wchar_t));
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, str, -1, wstring, len);
	wstring[len] = L'\0';

	// 逐个输出字符
	for(i=0; i<len; ++i)
	{
		wglUseFontBitmapsW(hDC, wstring[i], 1, list);
		glCallList(list);
	}

	// 回收所有临时资源
	free(wstring);
	glDeleteLists(list, 1);
}

void drawdesk(){
	glPushMatrix();
		glTranslated(0,-0.55,0);
	     glColor3f(1,1,1); 
	   glPushMatrix();
	     glColor3f(75/255.0,55/255.0,46/255.0);

         //your code here;
		 glTranslated(0,-0.6,8);
		 glScalef(5.0,0.3,3.2);//5-2.375
	     glutSolidCube(1);  //绘制凳子（背）

		 glColor3f(0.5,0.5,0.5);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(75/255.0,55/255.0,46/255.0);

         //your code here;
		 glTranslated(-2.3,-2.74,9);
		 glScalef(0.4,4.50,0.4);//5-2.375
	     glutSolidCube(1);  //绘制凳子（左前腿）

		 glColor3f(0.5,0.5,0.5);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(75/255.0,55/255.0,46/255.0);

         //your code here;
		 glTranslated(2.3,-2.74,9);
		 glScalef(0.4,4.50,0.4);//5-2.375
	     glutSolidCube(1);  //绘制凳子（右前腿）
		 //glutSolidTeapot(1.2);//测试，确定drawball的位置

		 glColor3f(0.5,0.5,0.5);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(75/255.0,55/255.0,46/255.0);

         //your code here;
		 glTranslated(-2.3,-2.74,7);
		 glScalef(0.4,4.50,0.4);//5-2.375
	     glutSolidCube(1);  //绘制凳子（左后腿）

		 glColor3f(0.5,0.5,0.5);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();

	   glPushMatrix();
	     glColor3f(75/255.0,55/255.0,46/255.0);

         //your code here;
		 glTranslated(2.3,-2.74,7);
		 glScalef(0.4,4.50,0.4);//5-2.375
	     glutSolidCube(1);  //绘制凳子（右后腿）
		 

		 glColor3f(0.5,0.5,0.5);
		 glutWireCube(1);	//线框模型
	   glPopMatrix();
	glPopMatrix();

}

void music(int mu) {
	if (mu == 1) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3back1);
	}
	if (mu == 2) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3back2);
	}
	if (mu == 3) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3back3);
	}
	if (mu == 4) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3back4);
	}
	if (mu == 5) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3back5);
	}
	if (mu == 6) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3back6);
	}
	if (mu == 7) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3back7);
	}
	if (mu == 8) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3backc1);
	}
	if (mu == 9) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3backc2);
	}
	if (mu == 10) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3backc3);
	}
	if (mu == 11) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3backc4);
	}
	if (mu == 12) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3backc5);
	}
	if (mu == 13) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3backc6);
	}
	if (mu == 14) {
		FSOUND_Stream_Play(FSOUND_FREE, mp3backc7);
	}
}

void drawball() {
	

	if (ballflag == 1) {
		for(int i = 0;i<k;i++){

			glPushMatrix();
			//your code here;
			glTranslated(line[i][0], line[i][1],0.3);
			glScalef(1, 1, 1);
			glutSolidSphere(0.5, 20, 20);
			
			glColor3f(colorball[i][0], colorball[i][1], colorball[i][2]);
			glPopMatrix();

		}


		
	}

}

void suiji() {
	if ((rand() % 2 + 1) % 2 == 0) {
		line[k - 1][0] = rand() % 4 + 1;		//随机生成x坐标
	}
	else {
		line[k - 1][0] = -(rand() % 4 + 1);
	}

	line[k - 1][1] = 5;

	colorball[k-1][0] = (float)(rand() % 100 + 1) / 100;
	colorball[k - 1][1] = (float)(rand() % 100 + 1) / 100;
	colorball[k - 1][2] = (float)(rand() % 100 + 1) / 100;
}

void stopmusic() {
	FSOUND_Stream_Stop(mp3back1);
	FSOUND_Stream_Stop(mp3back2);
	FSOUND_Stream_Stop(mp3back3);
	FSOUND_Stream_Stop(mp3back4);
	FSOUND_Stream_Stop(mp3back5);
	FSOUND_Stream_Stop(mp3back6);
	FSOUND_Stream_Stop(mp3back7);
	FSOUND_Stream_Stop(mp3backc1);
	FSOUND_Stream_Stop(mp3backc2);
	FSOUND_Stream_Stop(mp3backc3);
	FSOUND_Stream_Stop(mp3backc4);
	FSOUND_Stream_Stop(mp3backc5);
	FSOUND_Stream_Stop(mp3backc6);
	FSOUND_Stream_Stop(mp3backc7);
	x11 = 0, x12 = 0, x13 = 0, x14 = 0, x15 = 0, x16 = 0, x17 = 0;
	x21 = 0, x22 = 0, x23 = 0, x24 = 0, x25 = 0, x26 = 0, x27 = 0;
	musictime2 += 0.1;
}

void play(int pl) {
	for (int i = 0; i < 14; i++) {
		if (playerkey[i] == star[i]) {
			mathflag = 2;
			continue;
		}
		else {
			mathflag = 1;
			break;
		}
	}
	glutPostRedisplay();
}